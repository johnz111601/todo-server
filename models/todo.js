'use strict';

(async function init (){

    const
    base = require('path').basename(__filename).split('.')[0],
    mongoose = require('mongoose'),
    lib = require('../library'),
    ObjectId = mongoose.Types.ObjectId,

    Todo = module.exports = mongoose.model(base, mongoose.Schema({
        id:                     { type:Number, maxlength:7, required:true },
        date_added:             { type: Date, required:true, default: new Date() },
        added_by:               { type: Number, required:true},
        status:                 { type: Number, maxlength:1, required:true, default:1 },
        title:                  { type: String },
        note:                   { type: String }
        //basic info
     
    }, { strict: true })),

    fn = (name, callback) => {
        if(name && callback){
            module.exports[name] = (data, cb) => {
                switch(data){
                    case undefined: case null: case '':
                        return cb({ success: false,  message: "empty data" });
                        default:try{callback(data,cb);}catch(e){
                            return cb({success: false, message: "[Server ERROR] " +e.name+ " " +e.message});
                        }
                }
            };
        }
    };
   
    fn('addTodo', async (data, callback) => {
        data.added_by = data.auth.id;
        lib.create(data, (response) => {
            if (response.success) {
                lib.createEmit('todo');
                return callback({ success:true, message: "Added Successfully", data:response });
            } else {
                return callback({ success:false, message: "Failed to add", data:response });
            }
        }, base, 1000);
     });

    

    fn('updateTodo', (data, callback) => {
        Todo.findOneAndUpdate({ id: data.id }, { $set: { staus: data.statu, title: data.title, note: data.note } }, { upsert: true }, (err, response) => {
            if (err) return cb({ success: false, message: err.message });
            if (response) {
                lib.createEmit('todo');
                return callback({ success: true, message: "", data: response });
            } else {
                return callback({ success: true, message: "", data: response });
            }
        });
    });

    fn('toTask', (data, callback) => {
        Todo.findOneAndUpdate({ id: data.id }, { $set: { status: 1 , title: data.title, note: data.note } }, { upsert: true }, (err, response) => {
            if (err) return cb({ success: false, message: err.message });
            if (response) {
                lib.createEmit('todo');
                return callback({ success: true, message: "", data: response });
            } else {
                return callback({ success: true, message: "", data: response });
            }
        });
    });

    fn('toArchive', (data, callback) => {
        Todo.findOneAndUpdate({ id: data.id }, { $set: { status: 2 , title: data.title, note: data.note } }, { upsert: true }, (err, response) => {
            if (err) return cb({ success: false, message: err.message });
            if (response) {
                lib.createEmit('todo');
                return callback({ success: true, message: "", data: response });
            } else {
                return callback({ success: true, message: "", data: response });
            }
        });
    });



    fn('toTrash', (data, callback) => {
        Todo.findOneAndUpdate({ id: data.id }, { $set: { status: 3 } }, { upsert: true }, (err, response) => {
            if (err) return cb({ success: false, message: err.message });
            if (response) {
                lib.createEmit('todo');
                return callback({ success: true, message: "", data: response });
            } else {
                return callback({ success: true, message: "", data: response });
            }
        });
    });
   

    fn('getAllTodo', (data, callback) => {
        Todo.aggregate([
            {
                $match: {
                    added_by: data.auth.id,
                    status: data.status
                }
            },
        ]
            , (err, findResult) => {
                if (err) { return callback({ success: false, message: err.message }); } else {
                    return callback({ success: true, data: findResult });
                }
            });
    });


     fn('deletePermanent', (data, callback) => {
        Todo.remove({ id: data.id }, (err, response) => {
            if (err) return cb({ success: false, message: err.message });
            if (response) {
                lib.createEmit('todo');
                return callback({ success: true, message: "", data: response });
            } else {
                return callback({ success: true, message: "", data: response });
            }
        });
    });

    


    lib.createSocket( (client, io) => {
        return client.on('todo', (data) => {
            console.log(`'users' emit received at`, __filename, `with data "${data}"`);
            io.emit('todo', { success:true, data:data })
        });
    });
  

})();