'use strict';

(async function init (){

    const
    base = require('path').basename(__filename).split('.')[0],
    mongoose = require('mongoose'),
    lib = require('../library'),
    ObjectId = mongoose.Types.ObjectId,

    User = module.exports = mongoose.model(base, mongoose.Schema({
        id:                     { type:Number, maxlength:7, required:true },
        role:                   { type: String, maxlength: 20, required:true, default: 'admin'},
        firstName:              { type: String, maxlength: 150 }, 
        lastName:               { type: String, maxlength: 150 }, 
        username:               { type: String, maxlength: 256, required:true },
        password:               { type: String, required: true },
        secret:                 { type: String, maxlength: 100},
        session_id:             { type: String, maxlength:100, required:true },
        date_added:             { type: Date, required:true},
        status:                 { type: Number, maxlength:1, required:true, default:1 },
        isVerified:             { type: Boolean, default: false},
        isOnline:               { type: Boolean, default: false},
        //basic info
     
    }, { strict: true })),

    fn = (name, callback) => {
        if(name && callback){
            module.exports[name] = (data, cb) => {
                switch(data){
                    case undefined: case null: case '':
                        return cb({ success: false,  message: "empty data" });
                        default:try{callback(data,cb);}catch(e){
                            return cb({success: false, message: "[Server ERROR] " +e.name+ " " +e.message});
                        }
                }
            };
        }
    };
   

    fn('getUserById', (data, cb) => {
        User.findOne({id:  +data.id}, (err, response) => {
            if( err ) return cb({ success:false, message:err.message });
            if(response){
                return cb({ success:true, message: "User found!", data:response });
            }else{
                return cb({ success:false, message: "No user found!", data:response });
            }
        });
    });


    fn('createUser', async (data, callback) => {
        //console.log("User Data ID", data.auth.id);
        User.aggregate([
            {
                $project: {
                    _id: 1,
                    username: { $toLower: "$username" }
                }
            },
            {
                $match: {"username": data.username.toLowerCase()}
            }
        ], (err, res) => {
            if (err) return cb({ success: false, message: err.message });
            if (res.length == 0) {
                data.role = (()=>{
                    switch(data.role){
                        case 'admin': return 'admin';
                        default: return undefined;
                    }
                })();
                data.secret = data.password;
                data.email = data.username;
                data.date_added = new Date();
                data.status = 1;
                data.password = lib.createHash(data.password);
                data.secret = data.password;
                data.mobile = data.mobile;
                data.session_id = lib.createHash([Math.random(), Math.random(), Math.random()].join(''));
                lib.create(data, (response) => {
                    if(response.success){
                        lib.createEmit('users');
                        return callback({success: true, data: response, mesage: 'success'});
                    }else{
                        return callback({success: false, message: 'error creating userss'});
                    }
                }, base, 1);
            } else {
                return callback({ success:false, message: 'Username is not available' })
            }
        });
    });


    fn('authenticate', async (req, callback) => {
        let data = req.body;
        let userObj = { username: data.username };
        if( await lib.isUser(userObj) ){
            userObj.status = 1;
            let user = await getUser(userObj);
            if(user){
                    if( lib.compareHash(data.password, user.password) ){
                        let signInData = {
                            id: user.id,
                            username: user.username,
                            role: user.role,
                            status: user.status,
                            session_id: user.session_id,
                            firstName: user.firstName,
                            lastName: user.lastName,
                        };
                        lib.sign( signInData, response => {
                            if(response.success){
                                    return callback({ success:true, data: response.message });
                            }else{
                                return callback(response);
                            }
                        });
                    }else{
                        return callback({ success:false, message: 'Password incorrect', toaster: 'off' });
                    }
            }else{
                return callback({ success:false, message: 'Account has been disabled', toaster: 'off' });
            }
        }else{
            return callback({ success:false, message: 'User not found', toaster: 'off' });
        }
    });




    /**
     * @description Private model functions;
     * only accessible on this model file; 
     */

    /**
     * @param {Object} data object and values to check
     * @return {Object} User object data
     */

    function getUser(data){
        // console.log(data);
        return new Promise( (resolve ) => {
            if(Object.keys(data).length){
                User.findOne(data, (err, findOneResult) => {
                    if( err )  resolve(false);
                    resolve(findOneResult);
                });
            }else{
                resolve(false);
            }
        });
    }
    
    lib.createSocket( (client, io) => {
        return client.on('users', (data) => {
            console.log(`'users' emit received at`, __filename, `with data "${data}"`);
            io.emit('users', { success:true, data:data })
        });
    });
  

})();