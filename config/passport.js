const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/user');
const config = require('./database');
var ObjectId = require('mongodb').ObjectID;


module.exports = function(passport){
    let opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = config.secret;
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
    var query = { _id: jwt_payload._doc._id };
         User.findById(jwt_payload._doc._id, (err, user) => {
        //User.find(query, (err, user) => {      
            if(err){      
                return done(err, false);
            }
            if(user){
                return done(null, user); 
            }
            else{
                return done(null, false);
            }
        });
    }));
}

