'use strict';

module.exports = {
    database: 'mongodb://localhost:27017/todo',
    options: {
        useNewUrlParser: true 
    }
}