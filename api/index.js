'use strict';

(async function init (){

    const
        fs = require('fs'),
        routes = [];

        const readdirAsync = (path) => {
            return new Promise( (resolve, reject) => {
                fs.readdir(path, (error, files) => {
                    if (error) { reject(error); } else {
                        files.forEach(file => {
                            if(file.includes('.js') && !file.includes('index.js') && !file.includes('+'))
                                routes.push( require('./' + file) );
                        });
                        console.log( 'API endpoints are now running..' );
                        resolve(routes);
                    }
                });
            });
        }

    module.exports.start = async () => {
        return await readdirAsync('./api');
    };

})();