'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        _get('allUsers', async (req, res) => {
            await model.allUsers(req.body, response => {
                res.json(response)
            });
        });

        _post('authenticate', async (req, res) => {
            await model.authenticate(req, response => {
                res.json(response)
            });
        }, { private:false }); 

        _post('createUser', async (req, res) => {
            await model.createUser(req.body, response => {
                res.json(response)
            });
        }, {private:false});

    module.exports = lib.ROUTES(base);

 })();