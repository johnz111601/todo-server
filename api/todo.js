'use strict';

(async function init (){

    const
        base = require('path').basename(__filename).split('.')[0],
        lib = require('../library'),
        model = lib.modelInit(base),
        _get = lib.GET,  _post = lib.POST, _put = lib.PUT;

        _post('addTodo', async (req, res) => {
            await model.addTodo(req.body, response => {
                res.json(response)
            });
        });

        _get('getAllTodo', async (req, res) => {
            await model.getAllTodo(req.body, response => {
                res.json(response)
            });
        });

        _put('updateTodo', async (req, res) => {
            await model.updateTodo(req.body, response => {
                res.json(response)
            });
        });

        _put('toTrash', async (req, res) => {
            await model.toTrash(req.body, response => {
                res.json(response)
            });
        });

        _put('toArchive', async (req, res) => {
            await model.toArchive(req.body, response => {
                res.json(response)
            });
        });

        _put('toTask', async (req, res) => {
            await model.toTask(req.body, response => {
                res.json(response)
            });
        });

        _put('deletePermanent', async (req, res) => {
            await model.deletePermanent(req.body, response => {
                res.json(response)
            });
        });

    module.exports = lib.ROUTES(base);

 })();