'use strict';

(async function init (){

    const
        fs = require('fs'),
        private_key = fs.readFileSync('./config/private.key'),
        public_key = fs.readFileSync('./config/public.key'),
        router = require('express').Router(),
        Id = require('../models/id'),
        // Qid = require('../models/qid'),
        bcrypt = require('bcryptjs'),
        salt = bcrypt.genSaltSync(10),
        useragent = require('useragent'),
        jwt = require('jsonwebtoken');
        // nodemailer = require('nodemailer');

        let
            ROUTE,
            suckets = [],
            createEmit,
            LobbyClients = [];


    module.exports.createSocket = (suck) => {
        let exists = false;

        suckets.forEach( fn => {
            if( fn.toString() === suck.toString() ){
                exists = true;
            }
        });
        
        if(!exists)
        suckets.push(suck);
    }

    module.exports.i = () => {
        return {
            options: (express, app) => {
                const   cors = require('cors'),
                        bodyParser = require('body-parser'),
                        path = require('path'),
                        livePath = path.join(__dirname, '../app/dist'),
                        uploads = path.join(__dirname, '../uploads/images'),
                        avatars = path.join(__dirname, '../uploads/avatars');

                return (() => {
                    app.use(cors());
                    app.use(express.static(livePath));
                    app.use('/images', express.static(uploads));
                    app.use('/avatars/', express.static(avatars));
                    app.use(bodyParser.json({limit: '4mb'}));
                    app.use(bodyParser.urlencoded({limit:'4mb', extended: false}));
                    app.use((err, req, res, next) => {
                        if(err) res.json({ success:false, message: err.message });
                        next(); 
                    }); 

                });
            },
            logs: () => {
                const 
                    m = require('morgan'),
                    p = require('path'),
                    logs_path = !fs.existsSync(p.join(__dirname, '../logs'))?
                                fs.mkdirSync(p.join(__dirname, '../logs')):'',
                    stream = fs.createWriteStream(p.join(__dirname, '../logs/access.log'), { flags: 'a' });
                return {
                    start: () => {
                        console.log('Access logs running: logs/access.log');
                        return m((_, req, res) => {
                            return [
                                (new Date).toISOString(),
                                IP(req),
                                UA(req, res),
                                `[${getToken(req, 'id')},${getToken(req, 'username')},${getToken(req, 'role')}]`,
                                _.method(req, res),
                                _.url(req, res),
                                _.status(req, res),
                                _.res(req, res, 'content-length'), '-',
                                _['response-time'](req, res), 'ms', '\r\n\r\n'
                            ].join(' ')
                        }, { stream: stream });
                    }
                }
            },
            socket: (http, port) => {
                const io = require('socket.io')(http);
                createEmit = require('socket.io-client')
                            .connect(`http://localhost:${port}`);
                return {
                    start: () => {
                        let i=0;
                        io.on('connection', client => {  
                            suckets.forEach( sucks => {
                                sucks(client, io);
                            });

                            client.on('lobbyconnection', (data) => {
                                LobbyClients.push({socketid:data.socketid, uid:data.uid})
                                io.emit('lobbyconnection', { success:true, data:data })
                            });

                            client.on('lobbybroadcaster', (data) => {
                                io.emit('lobbybroadcaster', { success:true, data:data })
                            });

                            client.on('disconnect', client => {  
                                UpdateLobbySockets(Object.keys(io.sockets.sockets));
                            });
                        });
                        console.log(`[${suckets.length}] Socket(s) started..`);
                        return io;
                    }
                }
            },
            cronjobs: {
                start: () => {
                    // cS( (client, io) => {
                    //     return client.on('lobbybroadcaster', (data) => {
                    //         // console.log(`'lobbybroadcaster' emit received at`, __filename, `with data "${data}"`);
                    //         io.emit('lobbybroadcaster', { success:true, data:data })
                    //     });
                    // });

                    // User.aggregate([]);

                    
                    // setInterval(() => {
                    //     cE('lobbybroadcaster', Math.random() * 100)
                    //     // console.log('x');
                    // }, 3000)


                    return;
                }   
            }
        }
    }

    const UpdateLobbySockets = (activeUsers) => {
        let forDisconnection = LobbyClients.filter(e => activeUsers.indexOf(e.socketid) < 0);
        LobbyClients =  LobbyClients.filter(e => activeUsers.indexOf(e.socketid) > 0);
        forDisconnection.map( data => {
            require('../models/user')
            .updateOne(
                {
                    id: data.uid,
                    // "lobby.socket.socketid": data.socketid
                },
                {
                    $set: {
                        "lobby.socket": {
                            description: "offline",
                            lasthandshake: new Date,
                            isonline: false,
                            socketid: "",
                        }
                    }
                }, (err, forDisconnection) => {
                    if (err) return cb({ success: false, message: err.message });
                    if (forDisconnection.nModified) {
                        CE('lobbybroadcaster', { socketid:data.socketid, uid:data.uid });
                    }
                }
            );
        });
    }

    const getToken = module.exports.getToken = (req, key) =>{
        if( req.headers.authorization !== undefined ){
            let token = jwt.decode((req.headers.authorization).replace('bearer ', ''));
            if(key){
                for(let prop in token){
                    if(key === prop) return token[prop];
                }
                return '?';
            }else{
                return token;
            }
        }else{
            return '!';
        }
    }

    const CE = module.exports.createEmit = (emitTo, data = null) => {
        createEmit.emit(emitTo, data);
    }

    module.exports.newDate = (date = null) => {
        if( date ){
            let d = new Date(date);
            return new Date(d.setHours(d.getHours() + 8));
        }else{
            let d = new Date;
            return new Date(d.setHours(d.getHours() + 8));
        }
    }

    module.exports.randomString = (length) => {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    
    module.exports.randomNumber = (length) => {
        var result           = '';
        var characters       = '0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    module.exports.pincodeGenerator = (length) => {
        return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
    }

    

    module.exports.create = (data, callback, model, counter = 100001) => {
        if( data && model ){
            Id.generate(model, response => {
                if( response.success){
                    data.id = response.id;
                    require(`../models/${model}`).create(data, (err, createResult) => {
                        if(err) return callback({ success:false, message:err.message });
                        return callback({
                            success:true,
                            message: 'success',
                            data: createResult
                        });
                    });
                }else{
                    return callback({ success:false, message: `Cannot generate ${model} id` });
                }
            }, counter);
        }else{
            return callback({ success:false, message: "Cannot generate id (data or base not found!)" });
        }
    }

    // module.exports.createQID = (data, callback, counter = 1) => {
    //     if(data.docID) {
    //         Qid.generate(data.docID, response => {
    //             if( response.success){
    //                 data.id = response.id;
    //                 return callback({
    //                     success:true,
    //                     message: 'success',
    //                     data: response
    //                 });
    //             }else{
    //                 return callback({ success:false, message: `Cannot generate ${docID} id` });
    //             }
    //         }, counter)
    //     }
    // }


    /**
     * @param {String} password plain text password
     * @global salt is an autogenerated String hash.
     * @return {String} hash result
     */
    module.exports.createHash = (password) => {
        return bcrypt.hashSync(password.toString(), salt)
    }

    /**
     * @param {String} password plain text password
     * @param {String} hash hashed password from db
     * @return {Boolean} Match the plaint password and hash
     */
    module.exports.compareHash = (password, hash) => {
        return bcrypt.compareSync(password, hash)
    }

    /**
     * @param {String} role user role
     * @return {Boolean} if true; otherwise exit server
     * @description all user roles are defined here
     */
    module.exports.validateRole = (role, callback) => {
        let roles = 'superadmin admin user';
        if( roles.includes(role) ){
            return callback({ success:true });
        }else{
            return callback({ success:false });
        }
    }
    
    /**
     * @param {String} model route/model basepoint initializer
     * @return {Require} file to initialize
     */
    module.exports.modelInit = (model) => {
        let modelPath = `${__dirname}/../models/${model}.js`;
        if (fs.existsSync(modelPath)) {
            routerActivate(model);
            return require(modelPath);
        }else{
            exit(1, model);
        }
    }    

    /**
     * @param {Object} data user details
     * @param {Function} callback return
     * @return {Object} issued token or error mesages
     */
    module.exports.sign = (data, callback) => {
        try{
            let token;
            // i need to create non-expring token for clients - for API USE
            if(data.role == 'clients'){
                token = jwt.sign(data,private_key,{algorithm:'RS256'});
            }else{
                token = jwt.sign(data,private_key,{algorithm:'RS256',expiresIn: 60*60*24});
            }

            return callback({
                success: true,
                message: { token:token, user:data }
            });
        }catch(e){
            return callback({
                success: false,
                message: e.message,
            })
        }
    }

    /**
     * @param { String } data route basepoint
     * @return { Void } set ROUTE for activation
     */
    function routerActivate(data){
        ROUTE = data;
    }

    /**
     * @param { String } endpoint 
     * @param { Function } callback 
     * @description Overrides router.get() and injected jwt verification for authenticated reqeest
     */
    module.exports.GET = async (endpoint, callback, options = null) => {
        if( ROUTE ){

            router.get(`/${ROUTE}/${endpoint}`, (req, res) => {
                try{
                    if(req.headers.xdata) req.body = JSON.parse(req.headers.xdata);
                    routeOptions(req, res, callback, options);
                }catch(e){
                    res.json({ success:false, message:e.message });
                }
            });
            
            // defaultRoutes('get', ROUTE);
        }else{
            exit(0);
        }
    }

    /**
     * @param { String } endpoint 
     * @param { Function } callback 
     * @description Overrides router.post() and injected jwt verification for authenticated reqeest
     */
    module.exports.POST = async (endpoint, callback, options = null) => {
        if( ROUTE ){
            router.post(`/${ROUTE}/${endpoint}`, (req, res) => {
                routeOptions(req, res, callback, options);
            });
            // defaultRoutes('post', ROUTE);
        }else{
            exit(0);
        }
    }

    /**
     * @param { String } endpoint 
     * @param { Function } callback 
     * @description Overrides router.put() and injected jwt verification for authenticated reqeest
     */
    module.exports.PUT = async (endpoint, callback, options = null) => {
        if( ROUTE ){
            router.put(`/${ROUTE}/${endpoint}`, (req, res) => {
                routeOptions(req, res, callback, options);
            });
            // defaultRoutes('put', ROUTE);
        }else{
            exit(0);
        }
    }

    module.exports.ROUTES = (model) => {
        defaultRoutes(model);
        return router;
    }

    /**
     * @param {Request} req client/server request
     * @param {Response} res client/server response
     * @param {Function} callback return
     * @param {Object} options route protection private, allows
     * @return {Object} in json format
     */
    function routeOptions(req, res, callback, options = null){
        if( options && options.hasOwnProperty('private') ){
            if( options.private ){
                isVerified(req, response => {
                    if(response.success){
                        if(options.hasOwnProperty('allow')){
                            if(options.hasOwnProperty('allow') && options.allow.includes(req.body.auth.role)){
                                return callback(req, res, true);
                            }else{
                                res.json({success:false, message: `Access forbidden no access rights.`});
                            }
                        }else{
                            return callback(req, res, true);
                        }
                    }else{
                        res.json(response);
                    }
                }); 
            }else{
                return callback(req, res, true);
            }
        }
        else{
            isVerified(req, response => {
                if(response.success){
                    if(options && options.hasOwnProperty('allow')){
                        if(options.hasOwnProperty('allow') && options.allow.includes(req.body.auth.role)){
                            return callback(req, res, true);
                        }else{
                            res.json({success:false, message: `Access forbidden no access rights.`});
                        }
                    }else{
                        return callback(req, res, true);
                    }
                }else{
                    res.json(response);
                }
            }); 
        }
    }

    /**
     * @param {String} model set model api basepoint
     * @return {Void} manipulates router
     */
    function defaultRoutes(model){
        ['get', 'post', 'put'].forEach( method => {
            router[method](`/${model}/`, (req, res) => {
                res.json({
                    success: false,
                    message: 'Endpoint is disabled',
                    status: 0,
                    forwarder: {
                        'ip-address':  IP(req),
                        'user-agent': UA(req),
                        method: method,
                        body: req.body,
                    }
                });
            });
            
            router[method](`/${model}/*`, (req, res) => {
                res.json({
                    success: false,
                    message: 'Invalid endpoint',
                    status: 404,
                    forwarder: {
                        'ip-address':  IP(req),
                        'user-agent': UA(req),
                        method: method,
                        body: req.body,
                    }
                });
            });
        });
    }

    /**
     * @param {Request} req client/server request
     * @param {Function} callback return
     * @return {Object} in json format error or token
     */
    async function isVerified(req, callback){
        if( req.headers.authorization !== undefined ){
            jwt.verify(req.headers.authorization, public_key, async (err, verified) => {
                if (err) {
                    return callback({ success: false, message: 'Unauthorized access', codeMessage: err.message });
                } else {
                    req.body.auth = verified;
                    return await Passport(verified) ?
                        callback({ success: true,  message: 'ok', user:verified }):
                    callback({ success: false,  message: 'Unauthorized access', codeMessage: "User details didn't matched" });
                }
            });
        }else{
            //token is empty
            return callback({
                success: false,
                message: 'Unauthorized access',
                code: 7002,
                codeMessage: 'Token not found'
            });
        }
    }

    async function Passport(data){
        data = {
            $and: [
                { id: data.id },
                { status: data.status },
                { role: data.role },
                { username: data.username },
                { session_id: data.session_id }
            ]
        }
        return await isUser(data) ? true : false;
    }

    /**
     * @param {Object} data object and values to check
     * @return {Promise} Boolean true or false
     */
    const isUser = module.exports.isUser = (data) => {
        return new Promise( (resolve) => {
            if(Object.keys(data).length){
                require('../models/user')
                .findOne(data, (err, findOneResult) => {
                    if( err )  resolve(false);
                    resolve(findOneResult ? true : false);
                });
            }else{
                resolve(false);
            }
        });
    }

    /**
     * @param {Number} code equivalent error message
     * @param {String} message default null, your custom message
     * @return {Void} equivalent error codes
     */
    function exit(code, message = null){
        switch(code){
            case 0:
                console.log('\nError code: 0');
                console.log('Unable to access library function GET, POST & PUT requests.');
                console.log(`Please initialize`,  `\x1b[43m\x1b[30m`, `lib.routerActivate(base);`, `\x1b[0m\x1b[37m\n`);
            break;
            case 1:
                console.log('\nError code: 1');
                console.log('Cannot find model', `\x1b[43m\x1b[30m`, message, `\x1b[0m\x1b[37m\n`);
            break;
            case 2:
                console.log('\nError code: 2');
                console.log('fn.validateRole role', `\x1b[43m\x1b[30m`, message, `\x1b[0m\x1b[37m`, ` not found!\n`);
            break;
        }
        process.exit(0);
    }
    
    /**
     * @param {Request} req client/server request
     * @return {String} client ip address
     */
    const IP = module.exports.IP = (req) => {
        return req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress || 
            req.socket.remoteAddress ||
            (req.connection.socket ? req.connection.socket.remoteAddress : null);
    }

    /**
     * @param {Request} req client/server request
     * @return {String} client browser information
     */
    const UA = module.exports.UA = (req) => {
        let ua = useragent.parse(req.headers['user-agent']);
        return `${ua.toString()} / ${ua.device.toString()}`;
    }

    // module.exports.JSMailer = (emailAdd,pass,id) => {
    //     console.log(emailAdd, pass, id)
    //     async function main(email){
    //         // let testAccount = await nodemailer.createTestAccount();
    //         let transporter = nodemailer.createTransport({
    //             host: "smtp.googlemail.com",
    //             port: 465, //587,
    //             secure: true, // true for 465, false for other ports
    //             auth: {
    //                 user: 'tinkertechmailer@gmail.com', // gmail user
    //                 pass: 'T!nk3rMa!l3r2019'  // gmail password
    //             }
    //         });


    //         let htmlTemplate = `
    //             <h2>Verify this email account</h2>
    //             <p>To verify this account, please click the link below.</p>
    //             <p>And use this system generated password to login</p>
    //             <h1>`+pass+`</h1>
    //         `            

    //         let info = await transporter.sendMail({
    //             from: '"JAWEE"<tinkertechinc@gmail.com>', // sender address
    //             to: emailAdd, // list of receivers
    //             subject: "No Reply", // Subject line
    //             text: "Please Varify!", // plain text body
    //             html: htmlTemplate// html body
    //         });
    //         console.log("Message sent: %s", info.messageId);
    //         // console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));..
    //     }
        
    //     main().catch(console.error);
    // }

    // module.exports.JSMailerPasswordReset = (username, fname, code) => {
    //     console.log(username, fname, code)
    //     async function main(email){
    //         let transporter = nodemailer.createTransport({
    //             host: "smtp.googlemail.com",
    //             port: 465, //587,
    //             secure: true, // true for 465, false for other ports
    //             auth: {
    //                 user: 'tinkertechmailer@gmail.com', // gmail user
    //                 pass: 'T!nk3rMa!l3r2019'  // gmail password
    //             }
    //         });


    //         let htmlTemplate = `
    //             <h3>JAWEE</h3> <hr>
    //             <p>Hi `+ fname + ` </p>
    //             <p>We received a password request to your JAWEE account.</p>
    //             <p>Please enter the following security code:</p>
    //             <h1>`+code+`</h1>
    //         `
            

    //         let info = await transporter.sendMail({
    //             from: '"JAWEE"<security@tinkertechinc@gmail.com>', // sender address
    //             to: username, // list of receivers
    //             subject: "No Reply", // Subject line
    //             text: "Please Varify!", // plain text body
    //             html: htmlTemplate// html body
    //         });
    //         console.log("Message sent: %s", info.messageId);
    //     }
        
    //     main().catch(console.error);
    // }



    module.exports.passGenerator =(length) => {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
     }

})();